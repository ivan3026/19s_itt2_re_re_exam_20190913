#!/usr/bin/env python3

import yaml
import os

conf_file = "user.yml"

if __name__ == "__main__":

    with open(conf_file, 'r') as stream:
        cfg = yaml.safe_load(stream)

    try:
        print( "'first_name': First name is a string", str(cfg['first_name'] ) )
        print( "'room': room is a string", str(cfg['room'] ) )
        if not cfg['room'].startswith('A1.'):
                raise ValueError("room not a room number: '%s'"%cfg['room'])
    except Exception as ex:
        print( "Config file error")
        print( "Exception was: ", ex )
        exit(1)

    exit(0)

